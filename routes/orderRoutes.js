const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');

// Route to create order
router.post('/', auth.verify, orderController.createOrder);

// Route to retrieve orders
router.get('/', auth.verify, orderController.retrieveOrders);

module.exports = router;