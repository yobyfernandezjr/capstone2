const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Route for register
router.post('/register', userController.registerUser);

// Route for login authentication
router.post('/login', userController.loginUser);

// Route for all users details
router.get('/details', auth.verify, userController.getAllUsersDetails);

// Route for admin delegation
router.put('/admin', auth.verify, userController.setUserAdmin);

module.exports = router;