const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Route to create product (admin only)
router.post('/', auth.verify, productController.addProduct);

// Route to retrieve all products
router.get('/all', auth.verify, productController.getAllProducts);

// Route to retrieve singe product
router.get('/:productId', productController.getProduct);

// Route to update product information (admin only)
router.put('/:productId', auth.verify, productController.updateProduct);

// Route to archive product (admin only)
router.put('/:productId/archive', auth.verify, productController.archiveProduct);

module.exports = router;