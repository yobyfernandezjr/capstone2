// setup node js (express, mongoose, cors)
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// routes requirement here
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

// end route requirement

const app = express();

const port = process.env.PORT || 4000;

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI here:
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// end Main URI

// mongoose connection
mongoose.connect(`mongodb://yobynnam:admin1234@ac-za4ipyn-shard-00-00.ufiwlyz.mongodb.net:27017,ac-za4ipyn-shard-00-01.ufiwlyz.mongodb.net:27017,ac-za4ipyn-shard-00-02.ufiwlyz.mongodb.net:27017/e-commerceapi?ssl=true&replicaSet=atlas-x79x6p-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', () => {
	console.log('Connection Error');
});
db.once('open', () => {
	console.log('Connected to MongoDB!');
});

app.listen(port, () => {
	console.log(`API is now online at port ${port}.`);
});