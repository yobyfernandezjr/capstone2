const User = require('../models/user');
const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User registration
async function registerUser(req, res, next) {
	// const userPassword = bcrypt.hashSync(req.body.password, 10);
	const newUser = new User ({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10)
		// password: userPassword
	});
	try {
		const user = await User.findOne({email: req.body.email});
		if (user !== null) {
			return res.send('Email exists. Please login.');
		} else {
			await newUser.save();
			return res.send('Email is registered');
		};
	} catch (error) {
		next(error);
	};
};

// User login
async function loginUser(req, res, next) {
	try {
		const user = await User.findOne({email: req.body.email});
		if (user == null) {
			return res.send(false);
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
			if (isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(user)});
			} else {
				return res.send(false);
			};
		};
	} catch (error) {
		next(error);
	};
};

// All Users details
async function getAllUsersDetails (req, res, next) {
	const user = auth.decode(req.headers.authorization);
	// console.log(user)
	try {
		if (user.isAdmin) {
			const users = await User.find({}, {password: 0});
			return res.send(users);
		} else {
			const users = await User.findById(user.id);
			users.password = 'Password is private';
			res.send(users);
		}
	} catch (error) {
		next (error);
	};
}

// Set user to admin
async function setUserAdmin (req, res, next) {
	const user = auth.decode(req.headers.authorization);
	if (user.isAdmin) {
		try {
			result = await User.findByIdAndUpdate(req.body.userId, {isAdmin: req.body.isAdmin});
			return res.send('Change status priviledge successful.');
		} catch (error) {
			next (error);
		};
	} else {
		return res.send('Not authorized to change status.')
	};
};
	
module.exports = {
	registerUser: registerUser,
	loginUser: loginUser,
	getAllUsersDetails: getAllUsersDetails,
	setUserAdmin: setUserAdmin
};