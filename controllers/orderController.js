const Product = require('../models/product');
const Order = require('../models/order');
const User = require('../models/user');
const auth = require('../auth');

// Create order
async function createOrder (req, res, next) {
	const user = auth.decode(req.headers.authorization);
	// console.log(user.isAdmin)
	if (user.isAdmin) {
		return res.send('You are not allowed to order!');
	} else {
		try {
			let product = await Product.findOne({productId: req.body.productId})
			let newOrder = new Order ({
				userId: user.id,  
            	productId: req.body.productId,
            	productName: req.body.productName,
            	price: req.body.price,
            	quantity: req.body.quantity,
            	totalAmount: product.price * req.body.quantity
			});
			await newOrder.save();
			return res.send('Order created successfully!');
		} catch (error) {
			return next(error);
		};
	};
};

// Retrieve orders
async function retrieveOrders (req, res, next) {
	const user = auth.decode(req.headers.authorization);
	try {
		if (user.isAdmin) {
			const orders = await Order.find({});
			return res.send(orders);
		} else {
			const orders = await Order.find({userId: user.id});
			return res.send(orders);
		}
	} catch (error) {
		next (error);
	};
};

module.exports = {
	createOrder: createOrder,
	retrieveOrders: retrieveOrders
};