const Product = require('../models/product');
const User = require('../models/user');
const auth = require('../auth');

// create product (admin only)
async function addProduct (req, res, next) {
	const user = auth.decode(req.headers.authorization);
	if (user.isAdmin) {
		try {
			let newProduct = new Product ({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				quantity: req.body.quantity
			});
			await newProduct.save();
			return res.send('Product created successfully!');
		} catch (error) {
			return next(error);
		}
	} else {
		return res.send('You are not authorized!');
	}
};

// Retrieve all products
async function getAllProducts(req, res, next) {
	const user = auth.decode(req.headers.authorization);
	try {
		if (user.isAdmin) {
			const products = await Product.find({});
			return res.send(products);
		} else {
			const products = await Product.find({isActive: true});
			return res.send(products);
		}
	} catch (error) {
		next (error);
	};
};

// Retrive single product
async function getProduct (req, res, next) {
	try {
		console.log(Product)
		const product = await Product.findById(req.params.productId);
		// console.log(product)
		if (product) {
			return res.send (product);
		} else {
			return res.send('The product does not exists!')
		};
	} catch (error) {
		return next(error);
	};
};

// Update product information (admin only)
async function updateProduct (req, res, next) {
	const isAdminUser = auth.decode(req.headers.authorization).isAdmin;
	if (isAdminUser) {
		try {
			let product = await Product.findByIdAndUpdate(req.params.productId, {
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				quantity: req.body.quantity
			});
			await product.save();
			return res.send('Product updated!');
		} catch (error) {
			return next(error);
		};
	} else {
		return res.send ('Not authorized!');
	};
};

// Archive product (admin only)
async function archiveProduct (req, res, next) {
	const isAdminUser = auth.decode(req.headers.authorization).isAdmin;
	if (isAdminUser) {
		try {
			let product = await Product.findByIdAndUpdate(req.params.productId, {
				isActive: false
			});
			await product.save();
			return res.send('Product is sent to archive.');
		} catch (error) {
			return next(error);
		};
	} else {
		return res.send('Not authorized.');
	};
};

module.exports = {
	addProduct: addProduct,
	getAllProducts: getAllProducts,
	getProduct: getProduct,
	updateProduct: updateProduct,
	archiveProduct: archiveProduct
};